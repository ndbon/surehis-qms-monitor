import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigQmsComponent } from './config-qms.component';

describe('ConfigQmsComponent', () => {
  let component: ConfigQmsComponent;
  let fixture: ComponentFixture<ConfigQmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigQmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigQmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
