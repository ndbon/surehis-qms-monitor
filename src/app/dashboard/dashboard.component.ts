import { Component, OnInit, HostListener } from '@angular/core';
import * as moment from 'moment';
import axios from 'axios';
import * as socketio from 'socket.io-client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { QmsServiceService } from '../common/qms-service.service';
import { ModalConfig } from '../model/ModalConfig';

export enum KEY_CODE {
  ESC = 27 
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers:[QmsServiceService]
})
export class DashboardComponent implements OnInit {
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === KEY_CODE.ESC) {
      this.router.navigate(['/config-qms']);
    }   
  }
  private socket: SocketIOClient.Socket;
  public date:any='';
  public locate:any=''
  public patientQ:any=[];
  public resultQ:any=[];
  private mdConfig:ModalConfig;  
  private dataHighlight:any;
  constructor(private http: HttpClient,public router: Router, private qmsSvr:QmsServiceService) { }

  ngOnInit() {
    this.socket = socketio.connect(environment.urlNotify);
    this.socket.on('healthcare', (msg: any) => {
      console.log("socket healcare")
      this.loadData()
    });
     
    this.socket.on('idqueue', (msg: any) => {
      console.log("socket idqueue",msg)
      this.dataHighlight=msg
      this.loadData()
    });
    const now = moment().locale('vi');
    this.date=now.format('dddd') + ', ngày '+ now.format('L');  

    this.mdConfig=this.qmsSvr.getConfig();      
    if(this.mdConfig && this.mdConfig.TypeLocation !== 0 && this.mdConfig.ValueLocation !== 0){
        (this.mdConfig.TypeLocation == 1)?(this.paramData.RoomID=this.mdConfig.ValueLocation,this.paramData.HCRmBlockID=0, this.locate='Phòng '+this.mdConfig.ValueLocation):null;
        (this.mdConfig.TypeLocation == 2)?(this.paramData.HCRmBlockID=this.mdConfig.ValueLocation,this.paramData.RoomID=0, this.locate='Khối phòng '+this.mdConfig.ValueLocation):null;
        this.loadData()
    }
    else{
      this.router.navigate(['/config-qms']);
      // this.paramData.RoomID=0;
      // this.paramData.HCRmBlockID=0
    } 
    //this.loadData()
  }
  private paramData:any={
    "RoomID": 0,
    "HCRmBlockID": 0,
    "V_ConsultStatus": "701,708",
    "PageSize": -1,
    "PageNumber": 1
  }
  async loadData(){    
    let rec = await axios.post(environment.api+"/qms/GetListInfoHCQ",this.paramData );
    if(this.dataHighlight && rec.data && rec.data.lenght !== 0){
      rec.data.forEach(el => {
        if (this.dataHighlight.HCQueueID == el.HCQueueID){
          el.isHighlight=true;
        } 
      });
    }
    let dataforPtQ:any=rec.data.filter(x=>x.V_ConsultStatus == 701) // cho kham
    let dataforRsQ:any=rec.data.filter(x=>x.V_ConsultStatus == 708) // co ket qua cls 
    this.patientQ=[]
    this.resultQ=[]
    for (let i=0;i<8;i++){
      if(dataforPtQ[i]){
        this.patientQ.push({SequenceNo:dataforPtQ[i].SequenceNo,RoomName:dataforPtQ[i].RoomName,Contents:dataforPtQ[i].Contents,isHighlight:dataforPtQ[i].isHighlight})
      }
      else this.patientQ.push({SequenceNo:" ",RoomName:" ",Contents:" ",isHighlight:false})      
    }
    for (let i=0;i<8;i++){
      if(dataforRsQ[i]){
        this.resultQ.push({SequenceNo:dataforRsQ[i].SequenceNo,RoomName:dataforRsQ[i].RoomName,Contents:dataforRsQ[i].Contents,isHighlight:dataforRsQ[i].isHighlight})
      }
      else this.resultQ.push({SequenceNo:" ",RoomName:" ",Contents:" ",isHighlight:false})
      
    }    
  }

}
